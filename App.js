// import { StatusBar } from 'expo-status-bar';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, View, Platform, SafeAreaView } from 'react-native';

export default function App() {
  return (
    <View style={[styles.container, {
      flexDirection: "column"
    }]}>
      {
        Platform.OS === 'ios' ?
          <SafeAreaView
            backgroundColor="#5DF21F"
            translucent
            barStyle="dark-content"
          /> :
          <StatusBar
            backgroundColor="#5DF21F"
            translucent
            barStyle="dark-content"
          />
      }
      <View style={{ flex: 2, backgroundColor: "red", height: '100%', justifyContent: 'flex-end' }} >
        <View style={[styles.afterStatusBar,{ backgroundColor: "#7D07F2", justifyContent: 'center',alignItems: 'center'}]} >
          <View style={styles.wrapRectangle}>
            <View style={styles.circle}>
              <View style={styles.boxInCircle} />
            </View>
          </View>
          <View style={[styles.wrapRectangle, { alignItems: 'flex-end', justifyContent: 'space-around' }]}>
            <View style={styles.rectangle} />
            <View style={[styles.rectangle, { width: '50%' }]} />
          </View>
        </View>
      </View>
      <View style={{ flex: 1, flexDirection: "row", backgroundColor: "darkorange", height: '100%' }}>
        <View style={{ backgroundColor: "gray", width: "35%", height: '85%' }}>
          <View style={{ backgroundColor: "green", height: '50%' }}></View>
        </View>
        <View style={{ backgroundColor: "blue", width: "30%", height: '85%' }}>
          <View style={{ backgroundColor: "#5DF21F", width: '50%', height: '100%' }}></View>
        </View>
        <View style={{ backgroundColor: "gray", width: "35%", height: '85%', justifyContent: 'flex-end' }}>
          <View style={{ backgroundColor: "green", height: '50%' }}>
            <View style={{ backgroundColor: "#5DF21F", width: '50%', height: '100%' }}></View>
          </View>
        </View>
      </View>
      <View style={{ flex: 2, backgroundColor: "green", justifyContent: 'center' ,flexWrap:'wrap'}}>
        <View style={{ flexDirection: "row", justifyContent: 'space-evenly', alignItems: 'center', height: '40%' }}>
          <View style={styles.rectangleBottom} />
          <View style={styles.rectangleBottom} />
          <View style={styles.rectangleBottom} />
        </View>
        <View style={{ flexDirection: "row", justifyContent: 'space-evenly', marginTop: 16, height: '40%' }}>
          <View style={styles.rectangleBottom} />
          <View style={styles.rectangleBottom} />
          <View style={styles.rectangleBottom} />
        </View>
      </View>
      <View style={{ flex: 1, flexDirection: "row", backgroundColor: "darkorange", justifyContent: 'space-around', alignItems: 'center' }}>
        <View style={styles.circleBottom}></View>
        <View style={styles.circleBottom}></View>
        <View style={styles.circleBottom}></View>
        <View style={styles.circleBottom}></View>
      </View>
    </View>
  );
}
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 260 : 185;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  afterStatusBar:{
    height:APPBAR_HEIGHT
  },
  circle: {
    width: 90,
    height: 90,
    backgroundColor: 'white',
    margin: 'auto',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10
  },
  boxInCircle: {
    width: 50,
    height: 50,
    backgroundColor: 'blue'
  },
  wrapRectangle: {
    width: '50%',
    margin: 'auto',
    alignItems: 'flex-end',
    height: '40%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  rectangle: {
    width: '100%',
    height: '30%',
    backgroundColor: 'white',
  },
  rectangleBottom: {
    width: '27%',
    height: "100%",
    backgroundColor: 'white',
  },
  circleBottom: {
    width: 60,
    height: 60,
    backgroundColor: 'white',
    borderRadius: 50,
  },
});
